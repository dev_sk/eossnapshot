var request = require('request');
var stringify = require('csv-stringify');
var csv = require('csvtojson');
var fs = require('fs');


var csvFilePath = 'accounts.csv';
var eosEndpoint = 'http://eos.greymass.com';

csv()
        .fromFile(csvFilePath)
        .then(function(accounts) {
            console.log(`Got ${accounts.length} accounts`);
            process_accounts(accounts).then(function(results){              
                stringify(results, function(err, output) {

                  fs.writeFile('snapshot.csv', output, function (err) {
                      if (err) {
                          return console.log('Error creating CSV file!\n');
                      }
                      console.log('CSV file Successfully created!\n');
                  });

               });   
            }); 
        })


async function process_accounts(accounts){
   
   var results = [];
   for(var i in accounts){
       var account = accounts[i];
       var account_info =  await get_account(account.account_name);
        var balance = 0;
        if(account_info.account_name){         
           var liquid_balance = account_info.core_liquid_balance ? parseFloat(account_info.core_liquid_balance) : 0;

           var staked_total = account_info.voter_info ? (parseFloat(account_info.voter_info.staked) / 10000) : 0;

           var refund_net = (account_info.refund_request) ? parseFloat(account_info.refund_request.net_amount) : 0;
           var refund_cpu = (account_info.refund_request) ? parseFloat(account_info.refund_request.cpu_amount) : 0;
           var refund_total  = refund_cpu + refund_net;

           balance = liquid_balance + staked_total + refund_total;            
        }
        
        results.push({
            account_name: account.account_name,
            balance: Number(balance).toFixed(4)
        });
        console.log(`Processed ${results.length} accounts`);
   }
   return results;
}

function get_account(account){
    return new Promise(function(resolve, reject){
        try {
            request({
                   url: eosEndpoint + "/v1/chain/get_account",
                   method: "POST",
                   body: {account_name:account},
                   json: true
               }, function(error, response, body) {               
                   resolve(body);
               });
       } catch (err) {

       }    
    })     
}